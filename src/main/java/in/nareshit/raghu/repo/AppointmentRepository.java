package in.nareshit.raghu.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.nareshit.raghu.entity.Appoinment;

public interface AppointmentRepository extends JpaRepository<Appoinment, Long> {
}
